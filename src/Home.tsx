import React, {ChangeEvent} from "react";

type HomeState = {
    vmix: string
    name: string
    clicker: string
    icVisible: boolean
}

export default class Home extends React.Component<any, HomeState> {
    constructor() {
        super(null);
        this.state = {
            vmix: '',
            name: '',
            clicker: '',
            icVisible: false,
        };
    }

    handleVMixChange = (evt: ChangeEvent<HTMLInputElement>) => {
        this.setState({vmix: evt.target.value})
    }
    handleNameChange = (evt: ChangeEvent<HTMLInputElement>) => {
        this.setState({name: evt.target.value})
    }
    handleClickerChange = (evt: ChangeEvent<HTMLInputElement>) => {
        this.setState({clicker: evt.target.value})
    }
    handleICToggle = () => {
        this.setState({icVisible: !this.state.icVisible})
    }
    handleSubmit = () => {
        let {vmix, name, clicker} = this.state;
        this.props.history.push(`/${vmix}/${name}/${clicker}`);
    }

    render() {
        return (
            <div id="home">
                <form onSubmit={this.handleSubmit}>
                    <label htmlFor="vmix">Call ID</label>
                    <input name="vmix" type="text" required value={this.state.vmix}
                           onChange={this.handleVMixChange}/>
                    <label htmlFor="name">Display Name</label>
                    <input name="name" type="text" required value={this.state.name}
                           onChange={this.handleNameChange}/>
                    <input name="icToggle" type="button" onClick={this.handleICToggle} value="I'm presenting"/>
                    <div className={["collapsable", this.state.icVisible ? "expand" : "collapse"].join(" ")}>
                        <label htmlFor="clicker">Internet Clicker ID</label>
                        <input name="clicker" type="text" value={this.state.clicker}
                               onChange={this.handleClickerChange}/>
                    </div>
                    <hr/>
                    <input name="submit" type="submit" value="Connect"/>
                </form>
            </div>
        )
    }
}
