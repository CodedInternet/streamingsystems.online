import React from 'react';

export type InternetClickerProps = {
    code: string;
}

export default class InternetClicker extends React.Component<InternetClickerProps> {
    render() {
        return (
            <div id="internet-clicker">
                <iframe src={`https://internetclicker.com/?code=${this.props.code}`} frameBorder="0"
                        title="AOTV Internet clicker"/>
            </div>
        )
    }
}